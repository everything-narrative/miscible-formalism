# Miscible Formalism &mdash; Mixomal

A procedural/functional language with a powerful module system.

## Requirements and technologies

Mixomal is built on a bootstrapping toolchain that starts with Ruby (MRI, >=3.1) and generates LLVM IR (>=13.0).

To install the required gems, use Bundler (>=2.3).

