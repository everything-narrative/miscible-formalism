
class MonkeyPatching < UnitTest

  SPACE = (-100..100).to_a + [nil, nil, nil]
  def gen_range
    Range.new(*SPACE.sample(2, random: self.random).sort { |a,b| a&.<=>(b||a) || 0 }, random.rand(0..1)>0)
  end

  test def range_arithmetic_properties
    srand(42)
    a = b = c = nil
    100.times do
      a = gen_range()
      b = gen_range()
      c = gen_range()
      assert(a + b == b + a, "not communtative on #{a} #{b}")
      assert(a + (b + c) == (a + b) + c, "not associative on #{a} #{b} #{c}")
      z = a.exclude_end? ? 0...1 : 0..0
      assert(a + z == a, "not zero #{a}")
    end
  end

  test def range_arithmetic_consistency
    srand(42)
    100.times do
      a = gen_range()
      next if a.size.is_a? Float
      b = gen_range()
      next if b.size.is_a? Float
      x, y = random.rand(a), random.rand(b)
      assert((a + b).cover?(x + y), "#{a} + #{b} = #{a + b} domain violation #{x} + #{y} = #{x + y}")
    end
  end

  test def hash_inversions
    expect({a: 1, b: 1, c: 2, d: 2}).sent(:invert_multi).to_equal({1 => %i[a b], 2 => %i[c d]})
    expect({a: 1, b: 1, c: 1, d: 2, e: 2}).sent(:keys_at, 2).to_equal(%i[d e])
  end
end