
require './.rb'
require './invoke.rb'

class InvocationMetaprog < UnitTest

  class Example
    DESCRIPTION = "this is a test"
    include Invocation
    module Help;
      include Invocation;
      DESCRIPTION = "print help"
    end
    module Run;
      include Invocation;
      DESCRIPTION = "do stuff"
    end
    module Misc; end
    Constant = 2

    def run(arg1, arg2="", *arg3, arg4) end

    def help; end
    alias_method %i[--help -h -?], :help
    doc("print this message")

    attr_accessor :output
    alias_method %i[--output -o], :output=
    doc("print this message")
    
  end

  test def has_subcommands
    expect(Example.subcommands.keys.sort).to_equal(%w[help run].sort)
  end
  
  test def has_options
    expect(Example.options.keys.sort).to_equal(%w[--help -h -? -o --output].sort)
    expect(Example).sent(:match_option, "--help").holds()
  end
  
  test def has_posargs
    expect(Example.posargs.map(&:first)).to_equal(%i[arg1 arg2 arg3 arg4])
    expect(Example.pp_posargs).to_equal(%w"arg1 (arg2) [arg3] arg4")
    expect(Example.req_num_posargs).to_equal(2..nil)
  end

  test def short_help
    expect(Example.new("foo").short_help).to_satisfy do
      |arr| arr.last == Example::DESCRIPTION && arr.first in /foo/
    end
  end
end