
require './.rb'

module Invocation
  def self.included(base)
    base.extend(Invocation::Meta)
    base.const_set(:DOCUMENTATION, {})
  end

  DESCRIPTION = nil
  COMMAND_NAME = nil

  attr_accessor :arg0, :args
  alias_method :initialize, :arg0=

  def main(args)
    self.args = args.dup
    parse_args(args)
  rescue Errors::NoArguments
    puts short_help(nil)
    exit 2
  rescue Errors::MissingPositionalArgument => e
    puts short_help("#{e.uncamel}: #{e.message}")
    exit 2
  rescue Errors::ExtraPositionalArguments => e
    puts short_help("#{e.uncamel}: #{e.message}")
    exit 2
  end

  def parse_args(args)
    posargs = []
    ignore = false
    
    if args.empty? && !req.cover?(0) && subcommands.nonempty?
      raise Errors::NoArguments
    end
    while args.nonempty?
      parse_arg(args, posargs, ignore)
    end
  end

  def parse_arg(args, posargs, ignore = false)
    raise NotImplementedError
  end

  module Errors
    def uncamel(str)
      str.gsub(/([a-z])([A-Z])/) { $1 + " " + $2 }.downcase
    end
    UnrecognizedSubCommand, UnrecognizedOption =
      Enumerator.produce { Class.new(NameError).include(Errors) }
    OptionDoesNotTakeValue, OptionRequiresValue,
    NoArguments, MissingPositionalArgument,
    ExtraPositionalArguments =
      Enumerator.produce { Class.new(ArgumentError).include(Errors) }
  end

  def usage_string
    args = ["usage:", self.arg0]
    args << "<options>" if self.class.options.nonempty?
    args << "<subcommand> [...]" if self.class.subcommands.nonempty?
    args.concat(self.class.pp_posargs).join(" ")
  end

  def short_help(message=self.class::DESCRIPTION)
    [usage_string, message].compact
  end

  def subcommands_help
    commands = []
    widest = self.class.subcommands.keys.map(&:length).max
    self.class.subcommands.sort.each do
      |cmd, mod|
      commands << "\t#{cmd.ljust(widest)}\t#{mod::DESCRIPTION}"
    end
    commands
  end

  def options_help
    options = self.class.options.invert_multi
    options.transform_values! do
       |vs| vs.sort { |a, b| a.sub(/^--?/,'') <=> b.sub(/^--?/,'') }.join(' ')
    end
    widest = options.values.map(&:length).max
    options.sort.collect do
      |method, flags|
      "\t#{flags.ljust(widest)}\t#{self.class::DOCUMENTATION[method]}"
    end
  end

  def posargs_help
    posargs = self.class.pp_posargs
    widest = posargs.map(&:length).max
    posargs.each_with_index.collect do
      |arg, ix|
      "\t#{arg.ljust(widest)}\t#{self.class::DOCUMENTATION[ix+1]}"
    end
  end

  def long_help
    posargs = self.posargs_help
    subcoms = self.subcommands_help
    opts = self.options_help

    sections = short_help
    sections << "" if posargs.nonempty?
    sections.concat(posargs)
    sections << "" << "Subcommands:" if subcoms.nonempty?
    sections.concat(subcoms)
    sections << "" << "Options:" if opts.nonempty?
    sections.concat(opts)
  end

end

module Invocation::Meta

  OPTION_METHOD = %r"^-.$|^--..+$|^/.+$"

  def extended(object)
    if !object.is_a?(Invocation)
      object.arg0 += " #{name.split('::').last.downcase}"
    end
  end

  def command_name; self::COMMAND_NAME || self.name.split('::').last.downcase end

  def options
    instance_methods(false).collect do
      |mth| if mth in OPTION_METHOD
        [mth.to_s.gsub('_','-'), instance_method(mth).original_name]
      end
    end.compact.to_h
  end

  def match_option(opt)
    self.options.nil_for_empty&.fetch(opt) do
      raise Invocation::Errors::UnrecognizedOption, opt
    end&.then(&self.m(:instance_method))
  end

  def posargs
    if method_defined?(:run)
      instance_method(:run).parameters.map(&:reverse)
    else [] end
  end

  def req_num_posargs
    self.posargs.collect do
      |_, kind| case kind
        in :req then 1..1
        in :opt then 0..1
        in :rest then 0..nil
        else 0..0
      end
    end.reduce(0..0, :+)
  end

  def pp_posargs
    self.posargs.collect do
      |arg, kind|
      case kind
        in :req then arg.to_s
        in :opt then "(#{arg})"
        in :rest then "[#{arg}]"
        else nil
      end
    end
  end

  def match_posargs(args)
    req = self.req_num_posargs
    if req.begin && args.length < req.min
      raise Invocation::Errors::MissingPositionalArgument,
        self.posargs.select { |_, k| k == :req }[args.length].to_s
    elsif req.end && req.max < args.length
      raise Invocation::Errors::ExtraPositionalArguments, args[req.end..].join(" ")
    end
    args
  end

  def subcommands
    constants(false).collect do
      |cst|
      mod = const_get(cst)
      [mod::COMMAND_NAME || cst.to_s.downcase, mod] if mod.is_a?(Module) and mod < Invocation
    end.compact.to_h
  end

  def match_subcommand(com)
    self.subcommands.nil_for_empty &.fetch(com) do
      |com| raise Invocation::Errors::UnrecognizedSubCommand, com
    end
  end

  def method_added(mth)
    if mth in OPTION_METHOD
      orig_name = instance_method(mth).original_name
      define_singleton_method(:doc) do
        |str| self::DOCUMENTATION[orig_name] = str
        self.singleton_class.undef_method(:doc)
        orig_name
      end
    elsif mth == :run
      posarg = 1
      posargs = self.posargs.length
      define_singleton_method(:doc) do
        |str|
        self::DOCUMENTATION[posarg] = str
        posarg += 1
        self.singleton_class.undef_method(:doc) if posarg == posargs
      end
    end
  end

end
