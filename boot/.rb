class Object
  alias m method
  alias f! freeze
  alias f? frozen?
end

module Kernel
  def self.Proc(p)
    case
    when p.respond_to?(:to_proc) then p.to_proc
    when p.respond_to?(:call) then p.method(:call).to_proc
    when p.respond_to?(:deconstruct_keys) then p.deconstruct_keys.to_proc
    end
  end
end

class Module
  instance_method(:alias_method).then do
    |make_alias|
    define_method(:alias_method) do
      |aliases, base, *rest|
      *aliases, base = aliases, base, *rest if rest.nonempty?
      Array(aliases).each do
        |a| make_alias.bind(self).(a, base)
      end
    end
  end
end

module Boolean
  def self.included(base)
    return if base <= TrueClass
    return if base <= FalseClass
    raise "#{base} is not an subclass of #{Boolean}"
  end
  def self.prepended(base) raise TypeError end
end

class TrueClass
  include Boolean
  def to_i; 1; end
end
class FalseClass
  include Boolean
  def to_i; 0; end
end

require 'pathname'

class String
  def nonempty?; !empty? end
  def nil_for_empty; self unless self.empty? end
  def to_p; Pathname.new(self) end
end

class Pathname
  alias to_str to_path
  def to_p; self end
  def to_f; File.new(self) end
  def to_d; Dir.new(self) end
end

class Dir
  def to_p; Pathname.new(path) end
end

class File
  def to_p; Pathname.new(path); end
end

class Hash
  def nonempty?; !empty?; end
  def keys_at(*values)
    values = values.map {|v| [v,true]}.to_h
    result = []
    self.each do
      |k, v| result << k if values[v]
    end
    result
  end
  def invert_multi
    result = Hash.new { |h, k| h[k]=[] }
    self.each do
      |k, v| result[v] << k
    end
    result.default_proc = nil
    result
  end
  def nil_for_empty; self unless self.empty? end
end

class Array
  def nonempty?; !empty?; end
  def first=(val) self[0]=val; end
  def last=(val) self[-1]=val; end
  def nil_for_empty; self unless self.empty? end
end

NotSupportedError = Class.new(ScriptError)

class Range
  def +(other)
    lo = self.begin && other.begin && self.begin + other.begin
    ex = self.exclude_end?.to_i + other.exclude_end?.to_i
    hi = self.end && other.end && self.end + other.end - (ex>0).to_i
    Range.new(lo, hi, ex==2)
  end
end

class Message
  def initialize(name, *args, **kwargs, &block)
    raise TypeError, 'name must be a Symbol' unless name in Symbol
    @name = name
    @args = args
    @kwargs = kwargs
    @block = block
    self.f!
  end
  attr_reader :name, :args, :kwargs, :block
  def call(recv) recv.__send__(@name, *@args, **@kwargs, &@block) end
  def to_proc; proc do |recv| self.call(recv) end end
  def bind(recv) proc do self.call(recv) end end
  def to_a; @args end
  def deconstruct; @args end
  def to_h; @kwargs end
  def deconstruct_keys(_); @kwargs end
  def to_s; @name.to_s end
  def inspect "#<Message:#{self.format}>" end
  def format
    ".#{self.to_s}(#{self.format_argument_list})" +
    if @block then
      " { |#{self.format_block_parameters}| ... }"
    else "" end
  end
  private def format_argument_list
    @args.map(&:inspect).
    concat(@kwargs).map { |k,v| "#{k}: #{v.inspect}" }.
    join(', ')
  end
  private def format_block_parameters
    @block.parameters.map do |type, par|
      case type
        in :opt if @block.lambda? then "#{par}=..."
        in :req | :opt then par
        in :rest then "*#{par}"
        in :keyreq then "#{par}:"
        in :key then "#{par}:..."
        in :keyrest then "**#{par}"
      end
    end.join(", ")
  end
end