END { if $0 == __FILE__ then UnitTest.main(*ARGV) end }

require 'pathname'
require 'find'

class UnitTest
  
  def self.main(dir)
    Find.find(dir).map do
      |file|
      next if Pathname.new(file).extname != '.rb'
      require file
    end
    UnitTest.run() do
      |msg|
      puts msg
    end => total:, succeeded:, failed:
    puts if failed != 0
    puts "\t\t### SUMMARY ###"
    puts "\t\t#{plural(total, "test")} run in #{plural(COMPONENTS.length, "class", "es")}" 
    puts "\t\t#{plural(succeeded, "successful test")} (#{(100*succeeded).fdiv(total).round}%)" if succeeded > 0
    puts "\t\t#{plural(failed, "failed tests")} (#{(100*failed).fdiv(total).round}%)" if failed > 0
  end

  COMPONENTS = []
  
  def self.inherited(child)
    COMPONENTS << child
    child.const_set(:CASES, [])
    def child.test(name)
      self::CASES << self.instance_method(name)
    end
  end

  def initialize
    @token = Object.new.freeze
    @random = Random.new
  end
  attr_accessor :random
  def srand(seed) self.random = Random.new(seed); end

  def run()
    tests_run = 0
    self.class::CASES.each do
      |test_method|
      tests_run += 1
      file = Pathname.new(
        test_method.source_location.first
      ).relative_path_from(Dir.pwd)
      failure = catch @token do 
        test_method.bind(self).call
        nil
      end
      if failure then
        yield format_failure(failure, test_method)
      end
    end
    tests_run
  end

  def format_failure(failure, test_method)
    file = Pathname.new(
      test_method.source_location.first
    ).relative_path_from(Dir.pwd)
    [
      "#{'./' if file.relative?}#{file}:#{failure.lineno}: ",
      "#{self.class}##{test_method.name}",
      "\n\t#{failure.message}",
    ].join
  end

  def self.run()
    tests_run = 0
    failed_tests = 0
    COMPONENTS.each do
      |testclass|
      tests_run += testclass.new.run do
        |msg|
        yield msg
        failed_tests += 1
      end
    end
    return { total: tests_run, succeeded: tests_run - failed_tests, failed: failed_tests }
  end

  def self.plural(n, xs, s='s')
    "#{n} #{xs}#{s if n != 1}"
  end

  FailedTest = Struct.new(:lineno, :message)
  private def fail_test(msg, n=2)
    loc = caller_locations(n,1).first.lineno
    throw(@token, FailedTest.new(loc, msg))
  end

  def assert(cond, msg="")
    fail_test(msg) unless cond
  end

  def failure(msg)
    fail_test(msg)
  end

  def expect(recv)
    fail_test_method = method(:fail_test)
    Expectation.new(recv).instance_eval do
      define_singleton_method(:fail_test, &fail_test_method)
      self
    end
  end

  class Expectation

    def initialize(recv)
      @recv = recv
      @report = {}
      @report[:recv] = @recv.inspect 
      @report[:sent] = ""
      @report[:got] = ""
      @report[:exp] = ""
    end

    def sent(msg, *args, **kwargs, &block)
      @msg = msg
      @args = args
      @kwargs = kwargs
      @block = block
      @report[:sent] = self.class.format_message(@msg, *@args, **@kwargs, &@block)
      self
    end

    def self.format_message(msg, *args, **kwargs, &block)
      ".#{msg}#{
        if !args.empty? || !kwargs.empty? then
          "(#{
            [ args.map(&:inspect),
              kwargs.map { |k, v| "#{k}: #{v.inspect}" }
            ].flatten.join(', ')
          })"
        end
      }" + self.format_block(block)
    end

    def self.format_block(block)
      if block
        " {#{
          if !block.parameters.empty? then
          " |#{
            block.parameters.map do |type, par|
            case type
              in :opt if block.lambda? then "#{par}=..."
              in :req | :opt then par
              in :rest then "*#{par}"
              in :keyreq then "#{par}:"
              in :key then "#{par}:..."
              in :keyrest then "**#{par}"
            end
          end.join(", ")
          }|"
        end
        } ... }"
      else "" end
    end

    def render
      @report => recv:, sent:, got:, exp:
      "#{@invert ? "Unexpected" : "Expected"} #{recv}#{sent}#{exp}   # #{got}"
    end
  
    def not() @invert = !@invert; self end

    private def eval()
      @result ||= @recv.__send__(@msg || :itself, *Array(@args), **Hash(@kwargs), &@block)
    end

    private def got(thing)
      return got(thing, &:itself) unless block_given?
      @report[:got] = "=> " + (yield thing).inspect
      thing
    end

    private def check()
      @invert ^ if block_given? then yield eval else eval end
    end

    private def guard(cond=nil, &block)
      if block
        guard(begin
          check(&block)
        rescue => e
          @report[:got] = e.class.inspect
          false
        end)
      else
        fail_test(render(), 3) unless cond
      end
      nil
    end

    def to(&block)
      this = self
      Object.new.instance_exec {
        Test::Expectation::PREDICATES.each do
          |pred|
          define_singleton_method(pred.to_s.sub(/^to_/,'').intern, &this.method(pred))
        end
        define_singleton_method(:and_not, &this.method(:not))
        self
      }.instance_exec(&block)
      nil
    end

    def holds()
      @report[:exp] = ""
      guard() { |it| got(it) }
    end

    PREDICATES = [

    def to_be(pred, *args)
      @report[:exp] = self.class.format_message(pred, *args)
      guard() { |it| got(it).__send__(pred, *args) }
    end,

    def to_satisfy(&block)
      @report[:exp] = ".then #{self.class.format_block(block)}"
      guard() { |it| got(it).then(&block) }
    end,

    def to_be_a(mod)
      @report[:exp] = ".is_a?(#{mod})"
      guard() { |it| got(it, &:class).is_a?(mod) }
    end,
    alias_method(:to_be_an, :to_be_a),

    def to_equal(thing)
      @report[:exp] = " == #{thing}"
      guard() { |it| got(it) == thing }
    end,

    def to_match(thing)
      @report[:exp] = " in #{thing}"
      guard() { |it| got(it) in ^thing }
    end,

    def to_raise(ex = StandardError)
      @report[:exp] = ".then{false} rescue $! <= #{ex}"
      guard(begin
        check() { |it| got(it); false }
      rescue => e
        @report[:got] = e.class.inspect
        e.class <= ex
      end)
    end,
    ]
  end

end

# class Stub
#   @@stubs = Hash.new(0)
#   @@unstubs = Hash.new do
#     |klass|
#     trip_eq = klass.method(:===)
#     -> (_) do
#       next if (@@stubs[klass] -= 1) > 0
#       klass.define_singleton_method(:===, &triple_eq)
#     end
#   end
#   ITISASELF = -> (it) { it.is_a?(self) }

#   def initialize(klass=Object, log=:__log)
#     @class = klass
#     @log = []

#     define_singleton_method(log) { @log }
    
#     @@stubs[klass] += 1
#     ObjectSpace.define_finalizer(self, @@unstubs[klass])
#     klass.define_singleton_method(:===, &ITISASELF)
#   end
  
#   def class; @class end
#   def is_a?(klass) @class <= klass end

#   def ret(**kwargs)
#     singleton_class.undef_method(:ret)
#     kwargs.each do
#       |method, val|
#       define_singleton_method(method) do
#         |*args, **kwargs|
#         @log << [method, args, kwargs]
#         val
#       end
#     end
#   end

#   def err(**kwargs)
#     singleton_class.undef_method(:err)
#     kwargs.each do
#       |method, val|
#       define_singleton_method(method) do
#         |*args, **kwargs|
#         @log << [method, args, kwargs]
#         val
#       end
#     end
#   end
# end